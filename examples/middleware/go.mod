module github.com/fiorix/go-diameter/examples/middleware

go 1.15

require (
	gitlab.com/wicak/go-diameter v4.0.2
	github.com/opentracing/opentracing-go v1.2.0
)
