module gitlab.com/wicak/go-diameter

go 1.18

replace gitlab.com/wicak/go-diameter => gitlab.com/wicak/go-diameter v0.0.1

require (
	github.com/golang/glog v1.0.0
	github.com/golang/protobuf v1.5.2
	github.com/ishidawataru/sctp v0.0.0-20210707070123-9a39160e9062
	golang.org/x/net v0.0.0-20220722155237-a158d28d115b
	google.golang.org/grpc v1.48.0
)

require (
	golang.org/x/sys v0.0.0-20220520151302-bc2c85ada10a // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)
